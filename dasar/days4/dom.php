<!DOCTYPE html>
<html>
<head>
	<title>Memilih Elemen Berdasarkan Id</title>
</head>
<body>
<div id="tutorial"></div>
<script type="text/javascript">
	//mengakses element tutorial
	var tutorial = document.getElementById("tutorial");
	// mengisi teks ke dalam elemen
	tutorial.innerText = "Tutorial javascript";
	//memberikan css ke dalam elemen
	tutorial.style.backgroundColor = "blue";
	tutorial.style.padding = "10px";

</script>
</body>
</html>