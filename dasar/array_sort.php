
<html>
<head>
    <title>selamat datang</title>
</head>
<body>

    <h2>JAVASCRIPT ARRAY SORT</h2>
    <p>The reverse() method reverses the elements in an array.</p>
<p>By combining sort() and reverse() you can sort an array in descending order.</p>
<button onclick="myFunction()">Try it</button>
    <p id= "demo"></p>
    <script>
        var fruits = ["banana", "orange", "apple"];
        document.getElementById("demo").innerHTML = fruits;

        function myFunction(){
            fruits.sort();
            fruits.reverse();
            document.getElementById("demo").innerHTML = fruits;
        }

    </script>
</body>

</html>