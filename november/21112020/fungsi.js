var dataBarang = [
    "buku Tulis",
    "Pensil",
    "Spidol"
];

function showBarang(){
    var listBarang = document.getElementById("list-barang");
    //clear list barang
    listBarang.innerHTML= "";

    //cetak semua barang
    for (let i = 0; i<dataBarang.length;i++){
        var btnEdit = "<a href='#' onClick = 'editBarang("+i+")'>edit</a>";
        var btnHapus = "<a href='#' onClick = 'deleteBarang("+i+")'>Delete</a>";
        
        listBarang.innerHTML +="<li>"+dataBarang[i] + "["+btnEdit + "|"+btnHapus+"]</li>";

    
    }
}

function addBarang(){
    var input = document.querySelector("input[name=barang]");
    dataBarang.push(input.value);
    showBarang();
}

function editBarang(id){
    var newBarang = prompt("nama baru",dataBarang[id]);
    dataBarang[id]= newBarang;
    showBarang();

}

function deleteBarang(id){
    dataBarang.splice(id,1);
    showBarang();
}

showBarang();